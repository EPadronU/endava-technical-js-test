FROM --platform=x86-64 ubuntu:focal

LABEL name="endava-technical-js-test" \
	  maintainer="Edinson E. Padrón Urdaneta <edinson.padron@posteo.net>" \
	  version="1.0" \
	  description="Docker image to run the code associated to my assingment"

# Timezone to be used to configure tzdata
ARG tz=America/Bogota

# Configure tzdata
RUN ["/bin/bash", "-c", "ln -snf /usr/share/zoneinfo/${tz} /etc/localtime && echo ${tz} > /etc/timezone"]

# Update the package repositories
RUN ["/bin/bash", "-c", "apt update"]

# Installed required packages
RUN ["/bin/bash", "-c", "apt install g++ build-essential zip unzip git wget default-jre -y"]

# Install Google Chrome
RUN ["/bin/bash", "-c", "wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb \
                         && apt install ./google-chrome-stable_current_amd64.deb -y"]

# Allure Framework's version to be used
ARG allure_version=2.7.0

# Install Allure Framework
RUN ["/bin/bash", "-c", "wget https://dl.bintray.com/qameta/generic/io/qameta/allure/allure/${allure_version}/allure-${allure_version}.tgz \
                         && tar -zxvf allure-${allure_version}.tgz -C /opt/ \
                         && ln -s /opt/allure-${allure_version}/bin/allure /usr/bin/allure"]

# The user to be used to run the code
ARG user=wdio

# Create the user to be used to run the code
RUN ["/bin/bash", "-c", "groupadd -r  ${user} \
                         && useradd -r -g ${user} -G audio,video ${user} \
                         && mkdir -p /home/${user} \
                         && chown -R ${user}:${user} /home/${user}"]

# Stop using the root user
USER ${user}

# Use this user's home directory as the working directory
WORKDIR /home/${user}

# Install NVM
RUN ["/bin/bash", "-c", "wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.36.0/install.sh | bash"]

ENV NVM_DIR=/home/${user}/.nvm

RUN echo "source $NVM_DIR/nvm.sh" >> .bashrc

# Version of node to be used
ARG node_version=14.15.0

# Install NodeJS
RUN ["/bin/bash", "-c", "source $NVM_DIR/nvm.sh \
                         && nvm install v${node_version}"]

ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# Install the TypeScript compiler
RUN ["/bin/bash", "-c", "source $NVM_DIR/nvm.sh \
                         && npm install -g typescript"]