/*****************************************************************************/
import Page from "../core/Page";
import HomePage from "./HomePage";
/*****************************************************************************/

/**
 * An Airline's external page.
 */
export default class ExternalBookingPage extends Page {

    /**
     * @inheritdoc
     */
    getAtVerification(): boolean {
        return browser.waitUntil(() => !browser.getUrl().startsWith(HomePage.URL));
    }
}
