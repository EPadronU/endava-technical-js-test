/*****************************************************************************/
import { Element } from "@wdio/sync";
import Browser from "../core/Browser";
import Component from "../core/Component";
/*****************************************************************************/

/**
 * A section that allows to increase or decrease a particular kind of passenger.
 */
export default class TravelerQuantityComponent extends Component {

    private static readonly DECREMENTOR_SELECTOR = ".//button[contains(@class, 'decrementor-js')]";

    private static readonly INCREMENTOR_SELECTOR = ".//button[contains(@class, 'incrementor-js')]";

    private static readonly QUANTITY_SELECTOR = ".//input";

    private getDecrementor(): Element {
        return Browser.waitUntil(
            this.element.$(TravelerQuantityComponent.DECREMENTOR_SELECTOR),
            element => element.waitForClickable());
    }

    private getIncrementor(): Element {
        return Browser.waitUntil(
            this.element.$(TravelerQuantityComponent.INCREMENTOR_SELECTOR),
            element => element.waitForClickable());
    }

    private getQuantityInput(): Element {
        return Browser.waitUntil(
            this.element.$(TravelerQuantityComponent.QUANTITY_SELECTOR),
            element => element.waitForDisplayed());
    }

    setQuantity(targetQuantity: number) {
        if (targetQuantity < 0) {
            throw new Error("A positive quantity is required");
        }

        const quantityInput = this.getQuantityInput();
        const decrementor = this.getDecrementor();
        const incrementor = this.getIncrementor();

        while (Number(quantityInput.getValue()) < targetQuantity) {
            incrementor.click();
        }

        while (Number(quantityInput.getValue()) > targetQuantity) {
            decrementor.click();
        }
    }
}
