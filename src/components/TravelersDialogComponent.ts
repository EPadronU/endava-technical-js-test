/*****************************************************************************/
import Browser from "../core/Browser";
import Component from "../core/Component";
import TravelerQuantityComponent from "./TravelerQuantityComponent";
/*****************************************************************************/

/**
 * The modal view that handles the flights' passengers.
 */
export default class TravelersDialog extends Component {

    private static readonly ADULTS_SELECTOR = ".//label[text()='Adultos']/../..";

    private static readonly CHILDREN_SELECTOR = ".//label[text()='Niños']/../..";

    getAdultsQuantityComponent() {
        return new TravelerQuantityComponent(
            Browser.waitUntil(
                this.element.$(TravelersDialog.ADULTS_SELECTOR),
                element => element.waitForDisplayed()));
    }

    getChildrenQuantityComponent() {
        return new TravelerQuantityComponent(
            Browser.waitUntil(
                this.element.$(TravelersDialog.CHILDREN_SELECTOR),
                element => element.waitForDisplayed()));
    }
}
