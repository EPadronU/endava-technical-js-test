/*****************************************************************************/
import { Element } from "@wdio/sync";
import SearchBoxComponent from "../components/SearchBoxComponent";
import Browser from "../core/Browser";
import Page from "../core/Page";
/*****************************************************************************/

/**
 * Kayak's home page.
 */
export default class HomePage extends Page {

    private static readonly SEARCH_BOX_SELECTOR = ".title-section + section .Base-Search-SearchForm";

    static readonly URL = "https://www.kayak.com.co/";

    /**
     * @inheritdoc
     */
    getAtVerification(): boolean {
        return browser.waitUntil(() => browser.getUrl() === HomePage.URL);
    }

    /**
     * @inheritdoc
     */
    getUrl(): string {
        return HomePage.URL;
    }

    private getSearchBox(): Element {
        return Browser.waitUntil(
            browser.$(HomePage.SEARCH_BOX_SELECTOR),
            element => element.waitForDisplayed());
    }

    getSearchBoxComponent(): SearchBoxComponent {
        return new SearchBoxComponent(this.getSearchBox());
    }
}
