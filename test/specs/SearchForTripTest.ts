/*****************************************************************************/
import HomePage from "../../src/pages/HomePage";
import Browser from "../../src/core/Browser";
import SearchResultPage from "../../src/pages/SearchResultPage";
import { dateToDisplayDateFormat } from "../../src/utils/date";
/*****************************************************************************/

/* Next is the asignment describing what the test should do:

Go to https://www.kayak.com.co/
1. Search for a trip:
- From Medellín to San Francisco
- Depart date December 10, 2020
- Return Date January 2, 2021
- 2 Adults and 1 Child
2. After searching, update the departure date to December 15, 2020, and return date to January 5, 2021, and search again.
3. Select the option with the lowest price.
4. See the offer (it will redirect you to a new page)
5. Test ends at this point. Make sure to add all the validations you see fit to make sure execution was successful */

describe("automate a simple search for a flight", () => {
    const origin = "Medellín";
    const destination = "San Francisco";
    const departureFlight = "MDE ‐ SFO";
    const returnFlight = "SFO ‐ MDE";
    const adults = 2;
    const children = 1;
    const travelersTextContent = "3 pasajeros";
    const departureDate = new Date("2020-12-10T00:00");
    const returnDate = new Date("2021-01-02T00:00");
    const updatedDepartureDate = new Date("2020-12-15T00:00");
    const updatedReturnDate = new Date("2021-01-05T00:00");
    const loadMoreTimes = 3;

    let homePage: HomePage;

    it("should navigate to Kayak's home page", () => {
        homePage = Browser.toPage(() => new HomePage());
    });

    let searchResultPage: SearchResultPage;

    it("should search for the desired flight configuration and navigate to the result page", () => {
        searchResultPage = homePage
            .getSearchBoxComponent()
            .clearOrigin()
            .search(origin, destination, [departureDate, returnDate], adults, children);

        //close the alert popup that appears the first time I land at the result page
        searchResultPage.closePopUp();

        const searchBox = searchResultPage.getSearchBoxComponent();

        expect(searchBox.getOriginContent()).toContain(origin);

        expect(searchBox.getDestinationContent()).toContain(destination);

        expect(searchBox.getDepartureDateContent()).toEqual(dateToDisplayDateFormat(departureDate));

        expect(searchBox.getReturnDateContent()).toEqual(dateToDisplayDateFormat(returnDate));

        expect(searchBox.getTravelersContent()).toContain(travelersTextContent);
    });

    it("should search again with new departure and return dates", () => {
        searchResultPage = searchResultPage
            .getSearchBoxComponent()
            .search(undefined, undefined, [updatedDepartureDate, updatedReturnDate]);

        const searchBox = searchResultPage.getSearchBoxComponent();

        expect(searchBox.getOriginContent()).toContain(origin);

        expect(searchBox.getDestinationContent()).toContain(destination);

        expect(searchBox.getDepartureDateContent()).toEqual(dateToDisplayDateFormat(updatedDepartureDate));

        expect(searchBox.getReturnDateContent()).toEqual(dateToDisplayDateFormat(updatedReturnDate));

        expect(searchBox.getTravelersContent()).toContain(travelersTextContent);
    });

    it("should load some more offers", () => {
        for (let times = 0; times < loadMoreTimes; times++) {
            searchResultPage
                .getResultListComponent()
                .loadMoreResults();
        }
    });

    it("should have a cheapest offer which price is the lowest among all the shown results", () => {
        const resultList = searchResultPage.getResultListComponent();

        const listItems = resultList.getListItemComponents();

        const departureFlights = listItems.map(item => item.getDepartureFlight());

        expect(departureFlights.every(it => it.includes(departureFlight))).toBe(true);

        const returnFlights = listItems.map(item => item.getReturnFlight());

        expect(returnFlights.every(it => it.includes(returnFlight))).toBe(true);

        const itemPrices = listItems.map(item => item.getPriceValue());

        const cheapestPrice = resultList
            .getCheapestListItemComponent()
            .getPriceValue();

        expect(itemPrices.every(it => it >= cheapestPrice)).toBe(true);
    });

    it("should select the cheapest offer and navigate to an external page", () => {
        searchResultPage
            .getResultListComponent()
            .getCheapestListItemComponent()
            .goToBookingPageInANewTab(() => {
                // Take a screenshoot for the report
                browser.takeScreenshot();
            });
    });
});
