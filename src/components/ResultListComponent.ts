/*****************************************************************************/
import { Element } from "@wdio/sync";
import Browser from "../core/Browser";
import Component from "../core/Component";
import ResultListItemComponent from "./ResultListItemComponent";
/*****************************************************************************/

/**
 * The list of flights that match the search criteria that was introduced.
 */
export default class ResultListComponent extends Component {

    private static readonly CHEAPEST_SELECTOR = ".//div[contains(@class, 'bf-cheapest')]/ancestor::div[contains(@class, 'Flights-Results-FlightResultItem')]";

    private static readonly ITEMS_SELECTOR = ".Flights-Results-FlightResultItem";

    private static readonly RESULT_PAGINATOR = "//div[@class='resultsPaginator']/div[not(contains(@class,'loading'))]";

    getListItemComponents(): ResultListItemComponent[] {
        return this.element.$$(ResultListComponent.ITEMS_SELECTOR)
            .map(element => new ResultListItemComponent(element));
    }

    getCheapestListItemComponent(): ResultListItemComponent {
        return new ResultListItemComponent(Browser.waitUntil(
            this.element.$(ResultListComponent.CHEAPEST_SELECTOR),
            element => element.waitForDisplayed()));
    }

    private getResultPaginator(): Element {
        return Browser.waitUntil(
            browser.$(ResultListComponent.RESULT_PAGINATOR),
            element => element.waitForClickable());
    }

    loadMoreResults() {
        this.getResultPaginator().click();
    }
}
