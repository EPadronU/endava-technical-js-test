## About

Technical test assigned to me by Endava in order to showcase my QA automation
skills.

## Dependencies

You most have a couple of CLI (command line interface) tools before you can run
the code.

1. [nvm](https://github.com/nvm-sh/nvm#installing-and-updating) (to
   manage/install NodeJS) [Optional]
2. [NodeJS](https://nodejs.org/en/download/) (to run the code)
3. [TypeScript Compiler](https://www.typescriptlang.org#installation) (to
   compile to plain-old JS)
4. [Allure](https://docs.qameta.io/allure/#_installing_a_commandline) (to
   generate the reports)

## Docker image

In order to create the docker image to be used for CI/CD, we can execute
`docker build -t epadronu/endava-technical-js-test:latest .` at the
project's root.

## Commands

_Note: The following commands most be run at the project's root._

### Compiling the code

To generate the corresponding JS code for the TypeScript files, we can run the
command `npm run compile`.

### Running the tests

To execute the tests we can run the command `npm run test`.

### Getting the reports

To generate the reports we can run the command `npm run reports:generate`. To
visualize the reports (after they have been generated) we can execute the
command `npm run reports:open`, which will open a browser with the HTML
reports. To do both operations at once we can run the command `npm run
reports`.

### Linting

In order to run ESLint, a simple `npm run eslint` is enough.

### Cleaning up tasks

- To delete the TS build files we can run the command `npm run clean`.
- To delete the report's files we can execute the `npm run reports:clean`
  command.

## Triggering the pipeline

Such a thing can be done via a HTTP request, just like the example below:

``` bash
curl -X POST \
     -F token=TOKEN \
     -F ref=master \
     https://gitlab.com/api/v4/projects/22107299/trigger/pipeline
```

Please ask the repo's maintainer for the token.