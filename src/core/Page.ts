/**
 * Base class to model a view/page.
 */
export default abstract class Page {

    /**
     * Provides a verification to determine if the browser is at the right page.
     */
    getAtVerification(): boolean {
        return true;
    }

    /**
     * Provides this page's URL (if it has one).
     */
    getUrl(): string | undefined {
        return undefined;
    }
}
