/*****************************************************************************/
import { Element, WaitForOptions } from "@wdio/sync";
import Page from "./Page";
/*****************************************************************************/

/**
 * Group top-level interactions with the browser.
 *
 * Here lies the utility methods to ease every day tasks.
 */
export default abstract class Browser {

    private static readonly DEFAULT_TIMEOUT_IN_MILLIS = 30_000;

    private static readonly URL_ERROR = "The provided page doesn't have a URL";

    private static readonly VERIFICATION_ERROR = "The page failed it's implicit 'at' verification";

    private static readonly defaultWaitForOptions: WaitForOptions = {
        timeout: browser.config.waitforTimeout || Browser.DEFAULT_TIMEOUT_IN_MILLIS
    };

    /**
     * Wait for the condition to hold for this element or throw an error.
     *
     * @param element the element to be used for evaluating the condition
     * @param condition the condition to be evaluated
     * @param waitForOptions for customize how the waiting mechanism will behave
     */
    static waitUntil(
        element: Element,
        condition: (element: Element) => boolean,
        waitForOptions?: WaitForOptions): Element {
        browser.waitUntil(
            () => condition(element),
            waitForOptions || Browser.defaultWaitForOptions);

        return element;
    }

    /**
     * Navigate to a URL.
     *
     * @param url the URL to navigate to
     */
    static toUrl(url: string) {
        browser.url(url);
    }

    /**
     * Navigate to a page.
     *
     * @param pageSupplier a factory for the desired page
     */
    static toPage<P extends Page>(pageSupplier: () => P): P {
        const page: P = pageSupplier();

        if (page.getUrl() === undefined) {
            throw new Error(Browser.URL_ERROR);
        } else {
            browser.url(page.getUrl());
        }

        return Browser.atPage(() => page);
    }

    /**
     * Tell the browser it should be at the provided page.
     *
     * @param pageSupplier a factory for the desired page
     */
    static atPage<P extends Page>(pageSupplier: () => P): P {
        const page: P = pageSupplier();

        if (page.getAtVerification() === false) {
            throw new Error(Browser.VERIFICATION_ERROR);
        }

        return page;
    }

    /**
     * Execute the given block of code in the context of a different tab/window.
     *
     * @param newWindowContext the code to be executed
     */
    static switchToNewWindow(newWindowContext: () => void) {
        const windowHandles = browser.getWindowHandles();

        if (windowHandles.length === 1) {
            throw new Error("There's no other window to switch to");
        } else if (windowHandles.length > 2) {
            throw new Error("There's no way to determine what window to switch to");
        }

        const originalWindowHandle = browser.getWindowHandle();

        const targetWindowHandle = windowHandles
            .find(handle => handle !== originalWindowHandle);

        browser.switchToWindow(targetWindowHandle!);

        try {
            newWindowContext();
        } finally {
            browser.closeWindow();
            browser.switchToWindow(originalWindowHandle);
        }
    }
}
