/*****************************************************************************/
import { Element } from "@wdio/sync";
/*****************************************************************************/

/**
 * Base class to model a reusable UI component.
 */
export default abstract class Component {

    /**
     * The element to be used as the root for interacting with the component.
     */
    protected element: Element;

    /**
     * Create a new component with the given element as its root.
     *
     * @param element to be used as the root for interacting with the component
     */
    constructor(element: Element) {
        this.element = element;
    }
}
