/*****************************************************************************/
import { Element } from "@wdio/sync";
import ResultListComponent from "../components/ResultListComponent";
import SearchBoxComponent from "../components/SearchBoxComponent";
import Browser from "../core/Browser";
import Page from "../core/Page";
/*****************************************************************************/

/**
 * Kayak's search result page.
 */
export default class SearchResultPage extends Page {

    private static readonly TITLE_FRAGMENT = "Reserva:";

    private static readonly SEARCH_BOX_SELECTOR = "div[id*='-inlineSearch']";

    private static readonly CLOSE_POPUP_BUTTON_SELECTOR = "div[id$='dialog'][class*='R9-Overlay'][class*='visible'] [id$='close']";

    private static readonly RESULT_LIST_SELECTOR = "#searchResultsList";

    /**
     * @inheritdoc
     */
    getAtVerification(): boolean {
        return browser.waitUntil(() => browser
            .getTitle()
            .includes(SearchResultPage.TITLE_FRAGMENT));
    }

    private getSearchBox(): Element {
        return Browser.waitUntil(
            browser.$(SearchResultPage.SEARCH_BOX_SELECTOR),
            element => element.waitForDisplayed());
    }

    private getClosePopUpButton(): Element {
        return Browser.waitUntil(
            browser.$(SearchResultPage.CLOSE_POPUP_BUTTON_SELECTOR),
            element => element.waitForClickable());
    }

    getSearchBoxComponent(): SearchBoxComponent {
        return new SearchBoxComponent(this.getSearchBox());
    }

    getResultListComponent(): ResultListComponent {
        return new ResultListComponent(Browser.waitUntil(
            browser.$(SearchResultPage.RESULT_LIST_SELECTOR),
            element => element.waitForDisplayed()));
    }

    closePopUp() {
        this.getClosePopUpButton().click();
    }
}
