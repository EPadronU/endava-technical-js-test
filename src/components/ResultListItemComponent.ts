/*****************************************************************************/
import { Element } from "@wdio/sync";
import Browser from "../core/Browser";
import Component from "../core/Component";
import ExternalBookingPage from "../pages/ExternalBookingPage";
/*****************************************************************************/

/**
 * Each entry/flight/offer/option in the search result list.
 */
export default class ResultListItemComponent extends Component {

    private static readonly PRICE_SELECTOR = ".price-text";

    private static readonly SEE_OFFER_BUTTON_SELECTOR = "[id$='mb-best']";

    private static readonly DEPARTURE_FLIGHT_SELECTOR = "li.flight:nth-child(1) .section.duration .bottom";

    private static readonly RETURN_FLIGHT_SELECTOR = "li.flight:nth-child(2) .section.duration .bottom";

    private static readonly OFFER_OVERLAY_SELECTOR = "[id$='details-dialog'].visible";

    private getPrice(): Element {
        return Browser.waitUntil(
            this.element.$(ResultListItemComponent.PRICE_SELECTOR),
            element => element.waitForDisplayed());
    }

    private getSeeOfferButton(): Element {
        return Browser.waitUntil(
            this.element.$(ResultListItemComponent.SEE_OFFER_BUTTON_SELECTOR),
            element => element.waitForClickable());
    }

    getDepartureFlight(): string {
        return Browser
            .waitUntil(
                this.element.$(ResultListItemComponent.DEPARTURE_FLIGHT_SELECTOR),
                element => element.waitForDisplayed())
            .getText()
            .replace(/[ \n]+/gm, " ")
            .trim();
    }

    getReturnFlight(): string {
        return Browser
            .waitUntil(
                this.element.$(ResultListItemComponent.RETURN_FLIGHT_SELECTOR),
                element => element.waitForDisplayed())
            .getText()
            .replace(/[ \n]+/gm, " ")
            .trim();
    }

    getPriceValue(): number {
        return Number(this.getPrice()
            .getText()
            .replace("$", "")
            .replace(/\./g, ""));
    }

    goToBookingPageInANewTab(newTabContext: (page: ExternalBookingPage) => void) {
        this.getSeeOfferButton().click();

        Browser.switchToNewWindow(() => newTabContext(
            Browser.atPage(() => new ExternalBookingPage())));
    }
}
