/**
 * The box that allows to search for a specific flight.
 */
const LOCALE = "es";

export function dateToInputDateFormat(date: Date): string {
    const year = Intl.DateTimeFormat(LOCALE, { year: "numeric" })
        .format(date);

    const month = Intl.DateTimeFormat(LOCALE, { month: "2-digit" })
        .format(date);

    const day = Intl.DateTimeFormat(LOCALE, { day: "2-digit" })
        .format(date);

    return `${day}/${month}/${year}`;
}

export function dateToDisplayDateFormat(date: Date): string {
    const month = Intl.DateTimeFormat(LOCALE, { month: "numeric" })
        .format(date);

    const day = Intl.DateTimeFormat(LOCALE, { day: "numeric" })
        .format(date);

    const weekday = Intl.DateTimeFormat(LOCALE, { weekday: "short" })
        .format(date)
        .toLowerCase();

    return `${weekday} ${day}/${month}`;
}
