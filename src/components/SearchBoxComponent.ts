/*****************************************************************************/
import { Element } from "@wdio/sync";
import Browser from "../core/Browser";
import Component from "../core/Component";
import SearchResultPage from "../pages/SearchResultPage";
import { dateToInputDateFormat } from "../utils/date";
import TravelersDialog from "./TravelersDialogComponent";
/*****************************************************************************/

/**
 * The box that allows to search for a specific flight.
 */
export default class SearchBoxComponent extends Component {

    private static readonly DEPARTURE_BOX_SELECTOR = "div[id$='dateRangeInput-display-start']";

    private static readonly DEPARTURE_INPUT_SELECTOR = "div[id$='dateRangeInput-overlay-bg'][class$='visible'] + div div[id*='-depart'][id$='-input']";

    private static readonly DESTINATION_BOX_SELECTOR = "div[id$='-destination-airport-display']";

    private static readonly DESTINATION_INPUT_SELECTOR = "div[id$='-destination-airport-smarty-window']._ic3 input";

    private static readonly DESTINATION_ITEM_INFO_SELECTOR = "div[id$='-destination-airport-smarty-content'].visible .item-info";

    private static readonly ORIGIN_BOX_SELECTOR = "div[id$='-origin-airport-display']";

    private static readonly ORIGIN_INPUT_SELECTOR = "div[id$='-origin-airport-smarty-window']._ic3 input";

    private static readonly ORIGIN_ITEN_INFO_SELECTOR = "div[id$='-origin-airport-smarty-content'].visible .item-info";

    private static readonly RETURN_BOX_SELECTOR = "div[id$='dateRangeInput-display-end']";

    private static readonly RETURN_INPUT_SELECTOR = "div[id$='dateRangeInput-overlay-bg'][class$='visible'] + div div[id*='-return'][id$='-input']";

    private static readonly SUBMIT_BUTTON_SELECTOR = "[id$='-submit']";

    private static readonly TRAVELERS_BUTTON_SELECTOR = "[id*='-travelers']";

    private static readonly TRAVELERS_DIALOG_SELECTOR = "[id*='-travelers'][id$='-dialog_content'][style*='top']";

    private getOriginBox(): Element {
        return Browser.waitUntil(
            this.element.$(SearchBoxComponent.ORIGIN_BOX_SELECTOR),
            element => element.waitForClickable());
    }

    private getDestinationBox(): Element {
        return Browser.waitUntil(
            this.element.$(SearchBoxComponent.DESTINATION_BOX_SELECTOR),
            element => element.waitForClickable());
    }

    private getDepartureDateBox(): Element {
        return Browser.waitUntil(
            this.element.$(SearchBoxComponent.DEPARTURE_BOX_SELECTOR),
            element => element.waitForClickable());
    }

    private getReturnDateBox(): Element {
        return Browser.waitUntil(
            this.element.$(SearchBoxComponent.RETURN_BOX_SELECTOR),
            element => element.waitForClickable());
    }

    private getOriginInput(): Element {
        return Browser.waitUntil(
            browser.$(SearchBoxComponent.ORIGIN_INPUT_SELECTOR),
            element => element.waitForDisplayed());
    }

    private getDestinationInput(): Element {
        return Browser.waitUntil(
            browser.$(SearchBoxComponent.DESTINATION_INPUT_SELECTOR),
            element => element.waitForDisplayed());
    }

    private getDepartureDateInput(): Element {
        return Browser.waitUntil(
            browser.$(SearchBoxComponent.DEPARTURE_INPUT_SELECTOR),
            element => element.waitForDisplayed());
    }

    private getReturnDateInput(): Element {
        return Browser.waitUntil(
            browser.$(SearchBoxComponent.RETURN_INPUT_SELECTOR),
            element => element.waitForDisplayed());
    }

    private getSubmitButton(): Element {
        return Browser.waitUntil(
            this.element.$(SearchBoxComponent.SUBMIT_BUTTON_SELECTOR),
            element => element.waitForClickable());
    }

    private waitForOriginToBeFound(text: string): Element {
        return Browser.waitUntil(
            browser.$(SearchBoxComponent.ORIGIN_ITEN_INFO_SELECTOR),
            element => element.getText().includes(text));
    }

    private waitForDestinationToBeFound(text: string): Element {
        return Browser.waitUntil(
            browser.$(SearchBoxComponent.DESTINATION_ITEM_INFO_SELECTOR),
            element => element.getText().includes(text));
    }

    private getTravelersButton() {
        return Browser.waitUntil(
            this.element.$(SearchBoxComponent.TRAVELERS_BUTTON_SELECTOR),
            element => element.waitForClickable());
    }

    private getTravelersDialogComponent() {
        return new TravelersDialog(Browser.waitUntil(
            browser.$(SearchBoxComponent.TRAVELERS_DIALOG_SELECTOR),
            element => element.waitForDisplayed()));
    }

    clearOrigin(): SearchBoxComponent {
        this.getOriginBox().click();
        browser.keys(["Backspace"]);
        browser.keys(["Escape"]);
        return this;
    }

    fillInOrigin(origin: string) {
        this.getOriginBox().click();
        this.getOriginInput().addValue(origin);
        this.waitForOriginToBeFound(origin);
        browser.keys("Enter");
    }

    fillInDestination(destination: string) {
        this.getDestinationBox().click();
        this.getDestinationInput().addValue(destination);
        this.waitForDestinationToBeFound(destination);
        browser.keys("Enter");
    }

    fillInDates(departureDate: Date, returnDate: Date) {
        this.getDepartureDateBox().click();
        this.getDepartureDateInput().addValue(
            dateToInputDateFormat(departureDate));

        browser.keys(["Tab"]);

        this.getReturnDateInput().addValue(
            dateToInputDateFormat(returnDate));

        browser.keys(["Enter"]);
    }

    search(
        origin?: string,
        destination?: string,
        departureAndReturnDates?: [Date, Date],
        adultsQuantity?: number,
        childrenQuantity?: number): SearchResultPage {
        if (origin !== undefined) {
            this.fillInOrigin(origin);
        }

        if (destination !== undefined) {
            this.fillInDestination(destination);
        }

        if (departureAndReturnDates !== undefined) {
            this.fillInDates(departureAndReturnDates[0], departureAndReturnDates[1]);
        }

        if (adultsQuantity !== undefined || childrenQuantity !== undefined) {
            this.getTravelersButton().click();

            const travelersDialogComponent = this.getTravelersDialogComponent();

            if (adultsQuantity !== undefined) {
                travelersDialogComponent
                    .getAdultsQuantityComponent()
                    .setQuantity(adultsQuantity);
            }

            if (childrenQuantity !== undefined) {
                travelersDialogComponent
                    .getChildrenQuantityComponent()
                    .setQuantity(childrenQuantity);
            }

            browser.keys(["Escape"]);
        }

        this.getSubmitButton().click();

        return Browser.atPage(() => new SearchResultPage());
    }

    getOriginContent(): string {
        return this.getOriginBox().getText().trim();
    }

    getDestinationContent(): string {
        return this.getDestinationBox().getText().trim();
    }

    getDepartureDateContent(): string {
        return this.getDepartureDateBox().getText().trim();
    }

    getReturnDateContent(): string {
        return this.getReturnDateBox().getText().trim();
    }

    getTravelersContent(): string {
        return this.getTravelersButton().getText().trim();
    }
}
